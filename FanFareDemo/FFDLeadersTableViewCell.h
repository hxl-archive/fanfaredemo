//
//  FFDLeadersTableViewCell.h
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFDLeader.h"

@interface FFDLeadersTableViewCell : UITableViewCell

/**
 Set up the cell with for a leader.
 @param leader the target leader
 @returns void
 */
- (void)setup:(FFDLeader *)leader;

@end
