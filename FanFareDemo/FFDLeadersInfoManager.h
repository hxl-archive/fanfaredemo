//
//  FFDLeadersInfoManager.h
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FFDLeader.h"

@protocol FFDLeadersInfoReceiver <NSObject>

/**
 Return an array of leaders.
 @param leaders an array of leaders
 @returns void
 */
- (void)ffdHandleLeadersInfo:(NSArray *)leaders;

/**
 Return an error when loading fails.
 @param error the error
 @returns void
 */
- (void)ffdHandleLeadersInfoReceiverError:(NSError *)error;

@end

@interface FFDLeadersInfoManager : NSObject

/**
 Create and return a share instance.
 @returns the shared instance
 */
+ (instancetype)sharedInstance;

/**
 Fetch the leaders' infomation from the server.
 @param receiver the object that will receive the leaders information
 @returns void
 */
- (void)fetchLeadersInfo:(id<FFDLeadersInfoReceiver>)receiver;

/**
 Create the current user as a leader.
 @returns the current user; return nil if not exists
 */
- (FFDLeader *)currentUser;

@end
