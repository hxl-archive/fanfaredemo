//
//  FFDLeadersTableViewCell.m
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import "FFDLeadersTableViewCell.h"
#import "FFDOnlineResourceManager.h"

@interface FFDLeadersTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *leaderImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingSpinner;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (nonatomic) FFDLeader *currentLeader;

@end

@implementation FFDLeadersTableViewCell

- (void)setup:(FFDLeader *)leader
{
	self.currentLeader = leader;
	[self.imageLoadingSpinner startAnimating];
	self.leaderImageView.image = nil;
	self.nameLabel.text = leader.name;
	self.rankLabel.text = [NSString stringWithFormat:@"%@", leader.rank];
	self.scoreLabel.text = [NSString stringWithFormat:@"Score: %@", leader.score];
	
	[[FFDOnlineResourceManager sharedInstance] loadImageForLeader:leader
												  completionBlock:^(FFDLeader *originalLeader,
																	UIImage *image) {
		[self.imageLoadingSpinner stopAnimating];
		if (self.currentLeader == originalLeader) {
			self.leaderImageView.image = image;
		}
	}];
}

@end
