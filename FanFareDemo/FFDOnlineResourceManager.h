//
//  FFDOnlineResourceManager.h
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FFDLeader.h"

typedef void (^ LeaderImageLoadingBlock)(FFDLeader *leader, UIImage *);

@interface FFDOnlineResourceManager : NSObject

/**
 Create and return a share instance.
 @returns the shared instance
 */
+ (instancetype)sharedInstance;

/**
 Load the image for a leader.
 @param leader the leader needs to load its image
 @param completionBlock handle the image once it's loaded, cannot be nil
 @returns void
 */
- (void)loadImageForLeader:(FFDLeader *)leader completionBlock:(LeaderImageLoadingBlock)completionBlock;

@end
