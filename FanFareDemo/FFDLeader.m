//
//  FFDLeader.m
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import "FFDLeader.h"

@implementation FFDLeader

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	if (dict[@"id"] == nil
		|| dict[@"score"] == nil
		|| dict[@"rank"] == nil
		|| dict[@"name"] == nil
		|| dict[@"image_url"] == nil) {
		// hxl: only "access" is optional
		return nil;
	}
	
	self = [self init];
	
	if (self) {
		_ID = dict[@"id"];
		_score = dict[@"score"];
		_rank = dict[@"rank"];
		_name = dict[@"name"];
		_imageURL = [NSURL URLWithString:dict[@"image_url"]];
		_access = dict[@"access"];
	}
	
	return self;
}

- (NSString *)description
{
	return [NSString stringWithFormat:
			@"name[%@] ID[%@] score[%@] rank[%@] access[%@] \n image URL[%@]",
			self.name, self.ID, self.score, self.rank, self.access, self.imageURL];
}

@end
