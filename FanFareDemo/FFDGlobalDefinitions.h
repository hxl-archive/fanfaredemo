//
//  FFDGlobalDefinitions.h
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#ifndef FanFareDemo_FFDGlobalDefinitions_h
#define FanFareDemo_FFDGlobalDefinitions_h

#if DEBUG == 1
#define DBLOG	NSLog
#else
#define DBLOG	// hxl: do not print
#endif

#define G_URL_LEADERS_INFO	(@"https://keith.fanfareentertainment.com/api/v4/games/matching.json")

#endif
