//
//  FFDOnlineResourceManager.m
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import "FFDOnlineResourceManager.h"
#import "FFDGlobalDefinitions.h"

#if DEBUG == 1
#define DBCACHE	0
#else
#define DBCACHE	0
#endif

@interface FFDOnlineResourceManager ()

@property (nonatomic) NSCache *imageCache;

@end

@implementation FFDOnlineResourceManager

+ (instancetype)sharedInstance
{
	static dispatch_once_t onceToken;
	static id sharedObject;
	
	dispatch_once(&onceToken, ^{
		sharedObject = [[self alloc] init];
	});
	
	return sharedObject;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		self.imageCache = [NSCache new];
		
		// hxl: iOS doesn't purge NSCache upon receiving memory warning and it needs to be handled
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(removeAllCachedObjects:)
													 name:UIApplicationDidReceiveMemoryWarningNotification
												   object:nil];
	}
	return self;
}

- (void)removeAllCachedObjects:(NSNotification *)notification
{
	[self.imageCache removeAllObjects];
}

- (void)loadImageForLeader:(FFDLeader *)leader completionBlock:(LeaderImageLoadingBlock)completionBlock
{
    // hxl: TODO: Use more sophisticated image download implementation for the use case
    //            "the user is browsing a table with hundreds of elements, and cancel / defer image
    //            downloads if the table cell is out of screen" later.
    //            The simple dispatch_async belows works for now because the table we display right now
    //            only has 50 entries in it, and the effort is too much for a pre-interview screening...
    
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if (completionBlock) {
			
			// hxl: TODO: save images to /Library/Caches when there is a more defined caching policy
			
			UIImage *image = [self.imageCache objectForKey:leader.ID];
			if (image == nil) {
				NSURLRequest *request = [NSURLRequest requestWithURL:leader.imageURL];
				NSURLResponse *response = nil;
				NSError *error = nil;
				NSData *imageData = [NSURLConnection sendSynchronousRequest:request
														  returningResponse:&response
																	  error:&error];
				if (error == nil) {
					image = [UIImage imageWithData:imageData];
					if (image) {
#if DBCACHE == 1
						DBLOG(@"%s: save image for %@", leader.name);
#endif
						[self.imageCache setObject:image forKey:leader.ID];
					}
				}
			}
			else {
#if DBCACHE == 1
				DBLOG(@"%s: found cached image for %@", leader.name);
#endif
			}
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				completionBlock(leader, image);
			});
		}
		else {
#if DEBUG == 1
			[NSException raise:@"LoadImageForLeader Nil Completion Block Exception" format:@"leader: %@", leader];
#endif
		}
	});
}

@end
