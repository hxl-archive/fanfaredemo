//
//  FFDLeadersInfoManager.m
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import "FFDLeadersInfoManager.h"
#import "FFDGlobalDefinitions.h"

#if DEBUG == 1
#define DBJSON	0
#else
#define DBJSON	0
#endif

@interface FFDLeadersInfoManager ()

@property (nonatomic) FFDLeader *thisUser;

@end


@implementation FFDLeadersInfoManager

+ (instancetype)sharedInstance
{
	static dispatch_once_t onceToken;
	static id sharedObject;
	
	dispatch_once(&onceToken, ^{
		sharedObject = [[self alloc] init];
	});
	
	return sharedObject;
}

- (void)fetchLeadersInfo:(id<FFDLeadersInfoReceiver>)receiver
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		
		NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:G_URL_LEADERS_INFO]];
		NSURLResponse *response = nil;
		NSError *error = nil;
		NSData *jsonData = [NSURLConnection sendSynchronousRequest:request
												 returningResponse:&response
															 error:&error];
		if (error) {
			[receiver ffdHandleLeadersInfoReceiverError:error];
			return;
		}
		
#if DBJSON == 1
		NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		DBLOG(@"jsonString: %@", jsonString);
#endif
		
		NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
																 options:NSJSONReadingMutableContainers
																   error:&error];
		if (error) {
			[receiver ffdHandleLeadersInfoReceiverError:error];
			return;
		}
		
		NSArray *leadersInfo = jsonDict[@"leaders"];
		NSNumber *yourRank = jsonDict[@"your_rank"];
		NSNumber *yourScore = jsonDict[@"your_score"];
		
		if (leadersInfo == nil
			|| yourRank == nil
			|| yourScore == nil) {
			error = [NSError errorWithDomain:@"FFD_ERROR_BAD_FORMAT_JSON" code:0 userInfo:nil];
			[receiver ffdHandleLeadersInfoReceiverError:error];
			return;
		}
		
		self.thisUser = [[FFDLeader alloc] initWithDictionary:@{@"id":@"-1",	// hxl: TODO: don't know the ID of the current user, use -1 for now
																@"rank":yourRank,
																@"score":yourScore,
																@"name":@"This User",
																@"image_url":@""}];
		
		NSMutableArray *leaders = [NSMutableArray new];
		for (NSDictionary *dict in leadersInfo) {
			FFDLeader *leader = [[FFDLeader alloc] initWithDictionary:dict];
			if (leader) {
#if DBJSON == 1
				DBLOG(@"%@", leader);
#endif
				[leaders addObject:leader];
			}
			else {
				error = [NSError errorWithDomain:@"FFD_ERROR_BAD_FORMAT_LEADER_JSON" code:0 userInfo:nil];
				[receiver ffdHandleLeadersInfoReceiverError:error];
				return;
			}
		}
		
		
		[receiver ffdHandleLeadersInfo:[leaders sortedArrayUsingComparator:
										^NSComparisonResult(FFDLeader *a, FFDLeader *b) {
			return [a.rank compare:b.rank];
		}]];
	});
}

- (FFDLeader *)currentUser
{
	return self.thisUser;
}

@end
