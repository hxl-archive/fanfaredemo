//
//  FFDLeadersTableViewController.m
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import "FFDLeadersTableViewController.h"
#import "FFDLeadersInfoManager.h"
#import "FFDLeadersTableViewCell.h"

#define LEADER_TABLE_HEADER_VIEW_HEIGHT	(44.0)

@interface FFDLeadersTableViewController () <
	FFDLeadersInfoReceiver,
	UITableViewDataSource,
	UITableViewDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) UIView *leaderSectionHeaderView;
@property (nonatomic) NSArray *leaderArray;

@end

@implementation FFDLeadersTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationItem.title = @"Leaderboard";	// hxl: TODO: localize it
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[[FFDLeadersInfoManager sharedInstance] fetchLeadersInfo:self];
}


#pragma mark - UITableViewDataSource -

// hxl: Ideally should have used FFDLeadersInfoManager as the data source of the table view,
//      but I think it's overkill for a simple demo like this.

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.leaderArray count];
}
	
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	FFDLeadersTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FFDLeadersTableViewCell"];
	FFDLeader *leader = self.leaderArray[indexPath.row];
	[cell setup:leader];
	return cell;
}


#pragma mark - UITableViewDelegate -

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	FFDLeader *currentUser = [[FFDLeadersInfoManager sharedInstance] currentUser];
	
	if (section == 0 && currentUser != nil) {
		return LEADER_TABLE_HEADER_VIEW_HEIGHT;
	}
	else {
		return 0;
	}
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	FFDLeader *currentUser = [[FFDLeadersInfoManager sharedInstance] currentUser];
	
	if (section == 0 && currentUser != nil) {
		CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, LEADER_TABLE_HEADER_VIEW_HEIGHT);
		UILabel *headerView = [[UILabel alloc] initWithFrame:frame];
		headerView.backgroundColor = [UIColor darkGrayColor];
		headerView.textColor = [UIColor yellowColor];
		headerView.textAlignment = NSTextAlignmentCenter;
		headerView.text = [NSString stringWithFormat:@"Your Rank: %@ \t Score: %@", currentUser.rank, currentUser.score];	// hxl: TODO: localize it
		return headerView;
	}
	else {
		return nil;
	}
}


#pragma mark - FFDLeadersInfoReceiver -

- (void)ffdHandleLeadersInfo:(NSArray *)leaders
{
	self.leaderArray = leaders;
	[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)ffdHandleLeadersInfoReceiverError:(NSError *)error
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error domain]
													message:[error localizedDescription]
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

@end
