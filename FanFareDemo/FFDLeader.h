//
//  FFDLeader.h
//  FanFareDemo
//
//  Created by Haoxin Li on 9/27/14.
//  Copyright (c) 2014 Haoxin Li. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFDLeader : NSObject

@property (nonatomic, readonly) NSNumber *ID;	// hxl: ID might be alphanumeric in the future
@property (nonatomic, readonly) NSNumber *score;
@property (nonatomic, readonly) NSNumber *rank;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *access;
@property (nonatomic, readonly) NSURL *imageURL;

/**
 Initialize the leader with a given dictionary.
 @param dict a dictionary that represents a leader
 @returns the leader; returns nil if the dictionary is invalid
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
